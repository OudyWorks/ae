import http from 'http'
import {
    MongoDB,
    ElasticSearch,
    Redis,
    RabbitMQ
} from '@oudyworks/libraries'

const init = async () => {

    // Configure MongoDB

    console.log(`Configure MongoDB`)

    try {

        await MongoDB.configure('mongodb://localhost:27017/ae')

        console.log(`MongoDB configured and connected`)

    } catch (error) {

        console.log(`Error while configuring MongoDB`)

        throw error

    }

}

init().then(
    async () => {

        let server = http.createServer(),
            graph = require('./graph').default

        server.listen(8080)

        console.log(`Server is up`)

        graph(server)

        console.log(new Array(50).fill('-').join(''))

    }
).catch(
    error => console.log(error)
)