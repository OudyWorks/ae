import {
    GraphQL
} from '@oudyworks/libraries'
import falzy from 'falzy'
import {
    GraphQLObjectType,
    GraphQLID,
    GraphQLString,
    GraphQLBoolean
} from 'graphql'

class Phone extends GraphQL.entity.MongoDB {
    static async identify(phone) {
        let id = await this.isExistInRedis(phone, 'phone')
        if(!id)
            id = await this.insert({
                payload: {phone},
                phone: {phone},
                object: {},
                changes: ['phone']
            })
        return `${id}`
    }
    static async validate(state, object, errors) {

        if (state.phone !== undefined || !object._id) {

            state.phone = (state.phone || '').trim()

            if (falzy(state.phone))
                errors.phone = 'blank'

            else if(!state.phone.match(/^\+?[1-9]\d{1,14}$/))
                errors.phone = 'invalid'

        }
    }
    static async keyValidate(state, errors) {

        let id = ''

        await this.validate(state, {_id: ''}, errors)

        if(!errors.phone)
            id = await this.identify(state.phone)

        return id

    }
}

Phone.type = new GraphQLObjectType({
    name: 'Phone',
    fields() {
        return {
            id: {
                type: GraphQLID,
                resolve({_id}) {
                    return _id
                }
            },
            phone: {
                type: GraphQLString
            },
            verified: {
                type: GraphQLBoolean,
                resolve({verified, activated}) {
                    return activated || verified
                }
            },
            verification: {
                type: GraphQLString,
                resolve({verification, activation}) {
                    return activation || verification
                }
            }
        }
    }
})

Phone.redisRefs = [
    'id',
    'phone'
]

export default Phone