import {
    GraphQLObjectType,
    GraphQLID,
    GraphQLString
} from 'graphql'
import Entity from 'types/AE/Entity'
import Email from 'types/Email'
import Phone from 'types/Phone'

class Client extends Entity {
    static async validate(state, object, errors, context) {
        if (state.email !== undefined || !object._id) {

            state.email = await Email.keyValidate(state, errors)

        }

        if (state.phone !== undefined || !object._id) {

            state.phone = await Phone.keyValidate(state, errors)

        }
    }

}

Client.type = new GraphQLObjectType({
    name: 'Client',
    fields() {
        return {
            id: {
                type: GraphQLID,
                resolve({_id}) {
                    return _id
                }
            },
            name: {
                type: GraphQLString
            },
            address: {
                type: GraphQLString
            },
            email: {
                type: Email.type,
                resolve({email}) {
                    return Email.load(email)
                }
            },
            phone: {
                type: Phone.type,
                resolve({phone}) {
                    return Phone.load(phone)
                }
            }
        }
    }
})

export default Client