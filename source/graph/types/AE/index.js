import {
    GraphQL
} from '@oudyworks/libraries'
import {
    GraphQLObjectType,
    GraphQLID,
    GraphQLString
} from 'graphql'

import Email from 'types/Email'
import Phone from 'types/Phone'

class Ae extends GraphQL.entity.MongoDB {

}

Ae.type = new GraphQLObjectType({
    name: 'Ae',
    fields() {
        return {
            id: {
                type: GraphQLID,
                resolve({_id}) {
                    return _id
                }
            },
            name: {
                type: GraphQLString
            },
            address: {
                type: GraphQLString
            },
            email: {
                type: Email.type,
                resolve({email}) {
                    return Email.load(email)
                }
            },
            phone: {
                type: Phone.type,
                resolve({phone}) {
                    return Phone.load(phone)
                }
            },
            cin: {
                type: GraphQLString
            }
        }
    }
})

export default Ae