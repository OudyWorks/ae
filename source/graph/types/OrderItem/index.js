import {
    GraphQLObjectType,
    GraphQLID,
    GraphQLString,
    GraphQLFloat,
    GraphQLInt
} from 'graphql'
import Entity from 'types/AE/Entity'
import Item from 'types/Item'

class OrderItem extends Entity {

}

OrderItem.type = new GraphQLObjectType({
    name: 'OrderItem',
    fields() {
        return {
            id: {
                type: GraphQLID,
                resolve({_id}) {
                    return _id
                }
            },
            Item: {
                type: Item.type,
                resolve({item}, args, {ae}) {
                    return item.load(item, ae)
                }
            },
            quantity: {
                type: GraphQLInt
            },
            price: {
                type: GraphQLFloat
            }
        }
    }
})

export default OrderItem