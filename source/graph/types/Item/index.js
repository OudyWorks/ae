import {
    GraphQLObjectType,
    GraphQLID,
    GraphQLString,
    GraphQLFloat
} from 'graphql'
import Entity from 'types/AE/Entity'

class Item extends Entity {

}

Item.type = new GraphQLObjectType({
    name: 'Item',
    fields() {
        return {
            id: {
                type: GraphQLID,
                resolve({_id}) {
                    return _id
                }
            },
            name: {
                type: GraphQLString
            },
            description: {
                type: GraphQLString
            },
            price: {
                type: GraphQLFloat
            }
        }
    }
})

export default Item