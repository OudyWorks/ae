import {
    GraphQL
} from '@oudyworks/libraries'
import falzy from 'falzy'
import {
    GraphQLObjectType,
    GraphQLID,
    GraphQLString,
    GraphQLBoolean
} from 'graphql'

class Email extends GraphQL.entity.MongoDB {
    static async identify(email) {
        let id = await this.isExistInRedis(email, 'email')
        if(!id)
            id = await this.insert({
                payload: {email},
                email: {email},
                object: {},
                changes: ['email']
            })
        return `${id}`
    }
    static async validate(state, object, errors) {

        if (state.email !== undefined || !object._id) {

            state.email = (state.email || '').trim()

            if (falzy(state.email))
                errors.email = 'blank'

            else if(!state.email.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/))
                errors.email = 'invalid'

        }
    }
    static async keyValidate(state, errors) {

        let id = ''

        await this.validate(state, {_id: ''}, errors)

        if(!errors.email)
            id = await this.identify(state.email)

        return id

    }
}

Email.type = new GraphQLObjectType({
    name: 'Email',
    fields() {
        return {
            id: {
                type: GraphQLID,
                resolve({_id}) {
                    return _id
                }
            },
            email: {
                type: GraphQLString
            },
            verified: {
                type: GraphQLBoolean,
                resolve({verified, activated}) {
                    return activated || verified
                }
            },
            verification: {
                type: GraphQLString,
                resolve({verification, activation}) {
                    return activation || verification
                }
            }
        }
    }
})

Email.redisRefs = [
    'id',
    'email'
]

export default Email