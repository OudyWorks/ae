import {
    GraphQLObjectType,
    GraphQLID,
    GraphQLString,
    GraphQLFloat,
    GraphQLInt,
    GraphQLList
} from 'graphql'

import Entity from 'types/AE/Entity'
import Client from 'types/Client'
import OrderItem from 'types/OrderItem'

class Order extends Entity {

}

Order.type = new GraphQLObjectType({
    name: 'Order',
    fields() {
        return {
            id: {
                type: GraphQLID,
                resolve({_id}) {
                    return _id
                }
            },
            ref: {
                type: GraphQLString
            },
            client: {
                type: Client.type,
                resolve({client}, args, {ae}) {
                    return Client.load(client, ae)
                }
            },
            total: {
                type: GraphQLFloat
            },
            items: {
                type: new GraphQLList(OrderItem.type)
            },
            date: {
                type: GraphQLString
            }
        }
    }
})

export default Order