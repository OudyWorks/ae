import {
    GraphQLSchema,
    GraphQLObjectType,
    GraphQLString
} from '@oudyworks/libraries/graphql'

import AE from 'types/AE'
import Client from 'types/Client'
import Email from 'types/Email'
import Item from 'types/Item'
import Order from 'types/Order'
import OrderItem from 'types/OrderItem'
import Phone from 'types/Phone'

export default new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields() {
            return {

                ...AE.queries(),
                ...Client.queries(),
                ...Email.queries(),
                ...Item.queries(),
                ...Order.queries(),
                ...OrderItem.queries(),
                ...Phone.queries()

            }
        }
    }),
    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields() {
            return {

                ...AE.mutations(),
                ...Client.mutations(),
                ...Email.mutations(),
                ...Item.mutations(),
                ...Order.mutations(),
                ...OrderItem.mutations(),
                ...Phone.mutations()

            }
        }
    }),
    // subscription: new GraphQLObjectType({
    //     name: 'Subscription',
    //     fields() {
    //         return {
    //
    //             test: {
    //
    //             }
    //
    //         }
    //     }
    // })
})