import express from 'express'
import graphql from 'express-graphql'
import {
    graphiqlExpress,
} from 'apollo-server-express'
// import bodyParser from 'body-parser'
// import cors from 'cors'
import { execute, subscribe } from '@oudyworks/libraries/graphql'
import { SubscriptionServer } from 'subscriptions-transport-ws'
import schema from './schema'
import url from 'url'

const app = express()

app.use('/graphiql', graphiqlExpress({
    endpointURL: '/',
    //subscriptionsEndpoint: `wss://graph.sqweelty.com/`
}))

app.use('/', graphql(
    request => ({
        schema,
        context: {
            timestamp: +new Date()
        }
    })
))

export default server => {

    server.on('request', app)

    let subscriptionServer = new SubscriptionServer({
        execute,
        subscribe,
        schema
    }, {
        noServer: true
    })

    server.on(
        'upgrade',
        (request, socket, head) => {

            switch (url.parse(request.url).pathname) {

                case '/':

                    subscriptionServer.server.handleUpgrade(
                        request, socket, head,
                        ws =>
                            subscriptionServer.server.emit('connection', ws, request)
                    )

                    break

                default :

                    socket.destroy()

                    break

            }

        }
    )
}